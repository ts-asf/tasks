# README #

This is a repository with T-Systems ASF evaluation project.<br>
The project includes 3 tasks to be done in one Spring Boot application.

### Tasks ###
* [Task 1](task-1.md)
* [Task 2](task-2.md)
* [Task 3](task-3.md)

### Technology stack ###
* Java 11
* Spring Boot
* Gradle 6
* JUnit 5

### Requirements ### 
* Create a project with Spring Boot Web and gradle.
* Add a separate endpoint for each task.
* No other external libraries are allowed.
* You can add dependencies with scope "test" if it's needed to write new unit-tests.
* The input value for each task should be provided as the request parameter, which must be validated.
* Use json schema for the response.
* Provide detailed information in case of the error response.
* The code should be well documented.
* Provide test coverage for the application more than 80%.
* Provide check for test coverage during the test of the project. E.g. you can use [jacoco plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html).
* Endpoints in working application should be checked by Postman.
* No UI is needed.

### Result ###
* Author name : {PUT YOUR NAME HERE}
* Task 1 : {PUT EXAMPLE OF REQUEST HERE}
* Task 2 : {PUT EXAMPLE OF REQUEST HERE}
* Task 3 : {PUT EXAMPLE OF REQUEST HERE}

### How to start? ###
* Install [GIT](https://git-scm.com/) and [Gradle](https://gradle.org/)
* [Fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the repository
* You're ready to go

### What is initially provided? ###
* GitLab pipeline with build and test steps using gradle: [.gitlab-ci.yml](.gitlab-ci.yml).

### How can I submit the result? ###
* Make sure your code can be built (example command: "gradle clean assemble").
* Make sure that the check for the 80% test coverage is added to the test step of your project.
* Make sure all tests are green (example command: "gradle check").
* Start Spring Boot application and test your endpoints (e.g. using "gradle bootRun").
* Commit and push all changes to your repository.
* Check that the pipeline is green. We will not accept your solution if the pipeline is failing.
* Send us an email with the link to your repository. Be aware that the pipeline must be green all the time after you send us the link.

### Who do I talk to? ###
* In case of any questions contact the person who sent you the task.
