# Task 1 #

### General description ###
Write the given number in words.

### Input and expected output ###
* The input parameter is a string, that should contain the number in the integer range.
* The output is a json containing a string value.

### Examples ###
* The number 152 is written out in words: a hundred and fifty-two.
* The number 85,000 is written out in words: eighty-five thousand.
* The number 999,000 is written out in words: nine hundred and ninety-nine thousand.

The use of "and" when writing out numbers is in compliance with British usage.
