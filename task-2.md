# Task 2 #

### General description ###
Find the sum of all the multiples of the two given numbers below the given limit (individually for each given number and aggregatively for both numbers).
Multiple is the product of any quantity and an integer. I.e. for the quantities a and b, we say that b is a multiple of a if b = na for some integer n.

### Input and expected output ###
* There are 3 input parameters: the limit for the search and 2 numbers.
* The output is a json containing:
  - the sum of all the multiples of the first number below given limit,
  - the sum of all the multiples of the second number below given limit,
  - the sum of all the multiples of the first or the second number below given limit.

### Examples ###
* For limit 5 and numbers 2 and 3: the result is json, containing 6, 3 and 9.
* For limit 10 and numbers 2 and 3: the result is json, containing 20, 18 and 32.
* For limit 200 and numbers 5 and 7: the result is json, containing 3900, 2842 and 6217.
