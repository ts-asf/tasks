# Task 3 #

### General description ###
Check the string for the letter and number sequences and for maximum number of characters of the given patterns.

### Input and expected output ###
* There are 2 parameters: a string and a pattern list.
  - A string can contain English alphabet letters, special characters and numbers.
  - A patterns list can contain \d, \a, or \s. \d is a digit, \a is an English alphabet character, \s is a special character.
* The result is a json, containing:
  - an original string.
  - an array of all found letter or number sequences (sequences are e.g. abс, dcba, 123, 4321 - minimum 3 characters in a row).
  - an array each element of which contains the original pattern and the maximum number of characters matching this pattern.

### Examples ###
* For parameters qwer12qw and \a: the result is a json containing qwer12qw, the empty array and the array [{ "pattern":"\a", "max":4 }].
* For parameters abca123 and \d,\s: the result is a json containing abca123, the array ["abc", "123"] and the array [{ "pattern":"\d", "max":3 }, { "pattern":"\s", "max":0 } }].
* For parameters 12qwer#4321 and \a,\d,\s: the result is a json containing 12qwer#4321, the array ["4321"] and the array [{ "pattern":"\a", "max":4 }, { "pattern":"\d", "max":4 }, { "pattern":"\s", "max":1 }].
